import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { PATH_HOME, PATH_FORMULAR, PATH_DATA, PATH_HELP, PATH_LOGIN, PATH_REGISTER, PATH_USER, PATH_MAP, PATH_PRIVACY, PATH_RECOVERY } from "./routing-constants";
import { HomeComponent } from "./home/home.component";
import { SettingsFormComponent } from "./settings-form/settings-form.component";
import { DataComponent } from "./data/data.component";
import { HelpComponent } from "./help/help.component";
import { LoginComponent } from "./login/login.component";
import { AuthGuard } from "./services/auth.guard";
import { UserComponent } from "./user/user.component";
import { MapComponent } from "./map/map.component";
import { RegistrationComponent } from "./registration/registration.component";
import { PrivacyComponent } from './privacy/privacy.component';
import { PasswordRecoveryComponent } from './password-recovery/password-recovery.component';

const routes: Routes = [
  { path: "", redirectTo: PATH_LOGIN, pathMatch: "full" },
  {
    path: PATH_LOGIN,
    component: LoginComponent,
  },
  {
    path: PATH_PRIVACY,
    component: PrivacyComponent,
  },
  {
    path: PATH_USER,
    component: UserComponent,
    canActivate: [AuthGuard],
  },
  {
    path: PATH_HOME,
    component: HomeComponent,
    canActivate: [AuthGuard],
  },
  {
    path: PATH_DATA,
    component: DataComponent,
    canActivate: [AuthGuard],
  },
  {
    path: PATH_FORMULAR,
    component: SettingsFormComponent,
    canActivate: [AuthGuard],
  },
  {
    path: PATH_HELP,
    component: HelpComponent,
    canActivate: [AuthGuard],
  },
  {
    path: PATH_MAP,
    component: MapComponent,
    canActivate: [AuthGuard],
  },
  {
    path: PATH_REGISTER,
    component: RegistrationComponent,
  },
  { 
    path: PATH_RECOVERY,
    component: PasswordRecoveryComponent,
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
