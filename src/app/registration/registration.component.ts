import { Component, OnInit } from "@angular/core";
import { AuthService } from "../services/auth.service";
import { MatSnackBar } from "@angular/material/snack-bar";

@Component({
  selector: "app-registration",
  templateUrl: "./registration.component.html",
  styleUrls: ["./registration.component.scss"]
})
export class RegistrationComponent implements OnInit {

  constructor(public auth: AuthService, private _snackBar: MatSnackBar) {

  }

  ngOnInit() {
  }

  public register(Email: string, Password: string, Passwordrepeat: string, Username: string, Telefone: number, Address: string, Name: string, Surname: string) {
    if (Password === Passwordrepeat && Email.length !== 0) {
      this.auth.SignUp(Email, Password, Username, Telefone, Address, Name, Surname);
    } else {
      this._snackBar.open("Check Input", "OK", {
        duration: 2000,
      });
    }
  }

}
