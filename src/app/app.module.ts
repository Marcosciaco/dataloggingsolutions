import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { AngularFireModule } from "@angular/fire";
import { AngularFirestoreModule } from "@angular/fire/firestore";
import { AngularFireAuthModule } from "@angular/fire/auth";
import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { HomeComponent } from "./home/home.component";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { FlexLayoutModule } from "@angular/flex-layout";
import { SharedModule } from "./shared/shared.module";
import { TranslateLoader, TranslateModule } from "@ngx-translate/core";
import { TranslateHttpLoader } from "@ngx-translate/http-loader";
import { HttpClient, HttpClientModule } from "@angular/common/http";
import { SettingsFormComponent } from "./settings-form/settings-form.component";
import { DataComponent } from "./data/data.component";
import { HelpComponent } from "./help/help.component";
import { LoginComponent } from "./login/login.component";
import { UserComponent } from "./user/user.component";
import { AngularFireStorage } from "@angular/fire/storage";
import { FilesComponent } from "./files/files.component";
import { ChartsModule } from "ng2-charts";
import { MapComponent } from "./map/map.component";
import { RegistrationComponent } from "./registration/registration.component";
import { ReactiveFormsModule, FormsModule } from "@angular/forms";
import { PrivacyComponent } from './privacy/privacy.component';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { PasswordRecoveryComponent } from './password-recovery/password-recovery.component';

const config = {
  authDomain: "dataloggingsolutions.firebaseapp.com",
  projectId: "dataloggingsolutions",
  apiKey: "AIzaSyDJjp1v3XARmZuXg3uO4YG0ghkjmubO5L4",
  databaseURL: "https://dataloggingsolutions.firebaseio.com",
  storageBucket: "dataloggingsolutions.appspot.com",
  messagingSenderId: "sender-id",
  appId: "1:747449145200:web:2168f34f383c56a6",
};

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    SettingsFormComponent,
    DataComponent,
    HelpComponent,
    LoginComponent,
    UserComponent,    
    FilesComponent,
    MapComponent,
    RegistrationComponent,
    PrivacyComponent,
    PasswordRecoveryComponent,
  ],
  imports: [
    BrowserAnimationsModule,
    BrowserModule,
    SharedModule,
    ReactiveFormsModule,
    FormsModule,
    ChartsModule,
    AppRoutingModule,
    AngularFireModule.initializeApp(config),
    AngularFirestoreModule,
    AngularFireAuthModule,
    FlexLayoutModule,
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production }),

  ],
  providers: [AngularFireStorage],
  bootstrap: [AppComponent]
})
export class AppModule {}

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}

