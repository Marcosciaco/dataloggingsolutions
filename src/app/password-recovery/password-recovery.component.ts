import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { MatSnackBar } from '@angular/material';
import { TranslateService } from '@ngx-translate/core';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-password-recovery',
  templateUrl: './password-recovery.component.html',
  styleUrls: ['./password-recovery.component.scss']
})
export class PasswordRecoveryComponent implements OnInit {

  constructor(
    private translate: TranslateService,
    public auth: AuthService,
    private afAuth: AngularFireAuth,
    private snackBar: MatSnackBar
  ) { }

  ngOnInit() {
  }

  resetPassword(email: string) {
    this.translate.get("recoveryText").subscribe(sub => {
      this.snackBar.open(sub, "OK", {
        duration: 2000,
      });
    });
    return this.afAuth.auth.sendPasswordResetEmail(
      email,
      { url: "https://dataloggingsolutions.firebaseapp.com/__/auth/action" });
  }

}
