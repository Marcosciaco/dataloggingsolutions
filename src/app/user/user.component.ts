import { Component, OnInit } from "@angular/core";
import { AuthService } from "../services/auth.service";
import { User } from "../services/user.model";

@Component({
  selector: "app-user",
  templateUrl: "./user.component.html",
  styleUrls: ["./user.component.scss"]
})
export class UserComponent implements OnInit {

  constructor(public auth: AuthService) { }
  user: User;
  ngOnInit() {
  }

  change(usn, tel, usad, usfn, ussn) {
    this.auth.user$.subscribe(user => {
      this.user = user;
    });
    this.user.displayName  = usn;
    this.auth.updateUserData(this.user, usn, tel, usad, usfn, ussn);
  }

}
