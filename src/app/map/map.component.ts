import { Component, ViewChild, ElementRef, AfterViewInit } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { AngularFireStorage } from "@angular/fire/storage"; 
import {} from "googlemaps";


@Component({
  selector: "app-map",
  templateUrl: "./map.component.html",
  styleUrls: ["./map.component.scss"]
})
export class MapComponent implements AfterViewInit {

  public fileToUpload: File;
  public distance: number = 0;
  public labels = [];
  public alt = [];
  public lat = [];
  public long = [];
  public speed = [];
  public x = [];
  public y = [];
  public z = [];
  public records: any[] = [];
  public map: google.maps.Map;
  public mapProperties = {
    center: new google.maps.LatLng(46.40843200, 11.23804283),
    mapTypeId: "satellite",
    zoom: 5,
    maxZoom: 20
  };

  @ViewChild("csvReader", { static: true}) csvReader: any;
  @ViewChild("mapContainer", { static: false }) gmap: ElementRef;


  constructor(public dialog: MatDialog, public storage: AngularFireStorage) {
  }

  /**  */
  public ngAfterViewInit(): void {
    this.map = new google.maps.Map(this.gmap.nativeElement, this.mapProperties);
  }

  /**  */
  public uploadListener($event: any): void {
    const files = $event.srcElement.files;
    if (this.isValidCSVFile(files[0])) {
      const input = $event.target;
      const reader = new FileReader();
      reader.readAsText(input.files[0]);
      reader.onload = () => {
        const csvData = reader.result;
        const csvRecordsArray = ( csvData as string).split(/\r\n|\n/);
        this.records = this.getDataRecordsArrayFromCSVFile(csvRecordsArray);
      };
      reader.onerror = function() {
        console.log("error is occured while reading file!");
      };
      this.fileReset();
    } else {
      alert("Please import valid .csv file.");
      this.fileReset();
    }
  }

  /**  */
  public getDataRecordsArrayFromCSVFile(csvRecordsArray: any) {
    const csvArr = [];
    for (let i = 1; i < csvRecordsArray.length - 1; i++) {
      const curruntRecord = ( csvRecordsArray[i] as string).split(",");
      this.lat.push(Number(curruntRecord[0]));
      this.long.push(Number(curruntRecord[1]));
      this.alt.push(Number(curruntRecord[2]));
      this.speed.push(Number(curruntRecord[3]));
      this.x.push(Number(curruntRecord[4]));
      this.y.push(Number(curruntRecord[5]));
      this.z.push(Number(curruntRecord[6]));
    }

    this.map.setCenter(new google.maps.LatLng(this.lat[0], this.long[0]));
    this.map.setZoom(12)
    let col = this.setMarkers();

    for (let i = 1; i < csvRecordsArray.length; i++) {
      this.labels.push(String(i));
    }
    const destinations = [];
    for (let i = 0; i < this.lat.length; i++) {
      destinations.push(new google.maps.LatLng(this.lat[i], this.long[i]));
      //if(i != 0)
      //  this.distance += google.maps.geometry.spherical.computeDistanceBetween(new google.maps.LatLng(this.lat[i], this.long[i]), new google.maps.LatLng(this.lat[i-1], this.long[i-1]));
    }
    
    const ploylineOptions = {path: destinations, strokeColor: col};
    const ployline = new google.maps.Polyline(ploylineOptions);
    ployline.setMap(this.map);
    return csvArr;
  }
  
  /**  */
  public setMarkers(): string {
    const map = this.map;
    
    const x = this.x;
    const y = this.y;
    const alt = this.alt;
    const speed = this.speed;
    
    const maxAlt = this.indexOfMax(this.alt);
    const maxSpeed = this.indexOfMax(this.speed);
    const maxX = this.indexOfMax(this.x);
    const maxY = this.indexOfMax(this.y);
    const maxZ = this.indexOfMax(this.z);
    const color = this.getRandomColor();
    
    for (let i = 0; i < this.lat.length; i = i + 1) {
      this.createMarkerWithDialog(i, alt, speed, color, x, y, map, "assets/images/marker.png");
    }
    
    this.createMarkerWithDialog(maxAlt, alt, speed, color, x, y, map, "assets/images/max_alt.png");
    this.createMarkerWithDialog(maxSpeed, alt, speed, color, x, y, map, "assets/images/max_speed.png");
    this.createMarkerWithDialog(maxX, alt, speed, color, x, y, map, "assets/images/max_x.png");
    this.createMarkerWithDialog(maxY, alt, speed, color, x, y, map, "assets/images/max_y.png");
    this.createMarkerWithDialog(maxZ, alt, speed, color, x, y, map, "assets/images/max_z.png");

    this.createMarker("assers/images/start.png");
    this.createMarker("assers/images/ende.png");
    return color;
  }

  /**  */
  private createMarker(image: string) {
    const markerS = new google.maps.Marker({
      position: new google.maps.LatLng(this.lat[0], this.long[0]),
      icon: {
        url: image,
        scaledSize: new google.maps.Size(50, 50),
      }
    });
    markerS.setMap(this.map);
  }

  /**  */
  private createMarkerWithDialog(max: number, alt: any[], speed: any[], color: string, x: any[], y: any[], map: google.maps.Map<Element>, image: string) {
    const marker = new google.maps.Marker({
      position: new google.maps.LatLng(this.lat[max], this.long[max]),
      animation: google.maps.Animation.BOUNCE,
      icon: {
        url: image,
        scaledSize: new google.maps.Size(50, 50),
      }
    });
    marker.addListener("click", function () {
      MapComponent.getInfoDialog(alt[max], speed[max], color, x[max], y[max]).open(map, this);
    });
    marker.setMap(this.map);
  }

  /**  */
  isValidCSVFile(file: any) {
    return true; // #TODO
  }

  /**  */
  fileReset() {
    this.csvReader.nativeElement.value = "";
    this.records = [];
  }

  /**  */
  public indexOfMax(arr) {
    if (arr.length === 0) {
        return -1;
    }
    let max = arr[0];
    let maxIndex = 0;
    for (let i = 1; i < arr.length; i++) {
        if (arr[i] > max) {
            maxIndex = i;
            max = arr[i];
        }
    }
    return maxIndex;
  }

  /**  */
  public getRandomColor() {
    const color = Math.floor(0x1000000 * Math.random()).toString(16);
    return "#" + ("000000" + color).slice(-6);
  }

  /**  */
  public static getInfoDialog(altitude, speed, color, x, y): google.maps.InfoWindow {
    return new google.maps.InfoWindow({
      content:
      "<div id=\"content\" style=\"background-color: " + color + "\">" +
      "<div id=\"siteNotice\">" +
      "</div>" +
      "<div id=\"bodyContent\">" +
      "<p> Altitude: " + altitude + "m</p>" +
      "<p> Speed: " + speed + "km/h</p>" +
      "<p> X: " + x + "g</p>" +
      "<p> Y: " + y + "g</p>" +
      "</div>" +
      "</div>"
    });
  }
}
