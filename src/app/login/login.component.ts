import { Component, OnInit } from "@angular/core";
import { AuthService } from "../services/auth.service";
import { MatSnackBar } from "@angular/material/snack-bar";
import { AngularFireAuth } from "@angular/fire/auth";

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.scss"]
})
export class LoginComponent implements OnInit {

  constructor(public auth: AuthService, private afAuth: AngularFireAuth, private snackBar: MatSnackBar) { }

  ngOnInit() {
  }

  resetPassword(email: string) {
    this.snackBar.open("Password reset E-Mail has been sent", "OK", {
      duration: 2000,
    });
    return this.afAuth.auth.sendPasswordResetEmail(
      email,
      { url: "https://dataloggingsolutions.firebaseapp.com/__/auth/action" });
  }
}
