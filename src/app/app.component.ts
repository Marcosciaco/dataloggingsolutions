import { Component } from "@angular/core";
import { TranslateService } from "@ngx-translate/core";
import { Router } from "@angular/router";
import { AngularFireAuth } from "@angular/fire/auth";
import { AngularFirestore, AngularFirestoreDocument } from "@angular/fire/firestore";
import { User } from "../app/services/user.model";
import { Observable, of } from "rxjs";
import { switchMap } from "rxjs/operators";
import { auth } from "firebase";
import { PwaService } from './services/pwa.service';

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"]
})
export class AppComponent {

  user$: Observable<User>;

  constructor(
    _pwaService: PwaService,
    private translate: TranslateService,
    private afAuth: AngularFireAuth,
    private afs: AngularFirestore,
    private router: Router
    ) {
      this.user$ = this.afAuth.authState.pipe(
        switchMap(user => {
          if (user) {
            return this.afs.doc<User>(`users/${user.uid}`).valueChanges();
          } else {
            return of(null);
          }
        })
      );
      translate.setDefaultLang("de");
      translate.addLangs(["de", "it", "en"]);
      const browserLang = translate.getBrowserLang();
      translate.use(browserLang.match(/de|en|it/) ? browserLang : "en");
  }

  title = "DataLoggingSolutions";
}
