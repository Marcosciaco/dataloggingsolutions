import { Component, OnInit, ViewChild } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { AngularFireStorage } from "@angular/fire/storage";
import { ChartDataSets, ChartOptions } from "chart.js";
import { Label, BaseChartDirective } from "ng2-charts";

@Component({
  selector: "app-data",
  templateUrl: "./data.component.html",
  styleUrls: ["./data.component.scss"]
})
export class DataComponent implements OnInit {

  public fileToUpload: File;
  public count = 0;
  public labels = [];
  public maxspeed;
  public speed = [0];
  public maxx;
  public x = [0];
  public maxy;
  public y = [0];
  public lineChartData: ChartDataSets[] = [];
  public lineChartLabels: Label[] = this.labels;
  public lineChartOptions: (ChartOptions) = {
    responsive: true,
    scales: {
      // We use this empty structure as a placeholder for dynamic theming.
      xAxes: [{}],
      yAxes: [{}]
    },
    legend: {
      position: "bottom",
      labels: {
        fontSize: 20,
        usePointStyle: false,
        padding: 10
      }
    },
    elements:
    {
        point:
        {
            radius: 0,
            hitRadius: 5,
            hoverRadius: 10,
            hoverBorderWidth: 2
        }
    },
    animation: {
      duration: 1, // general animation time
  },
  hover: {
      animationDuration: 1, // duration of animations when hovering an item
  },
  responsiveAnimationDuration: 1, // animation duration after a resize
  };

  public lineChartColors: string[] = [
    "#abf9ff", "#97edff", "#5fdbff",
    "#f9eed9", "#f0e4b1", "#ceaf67",
    "#ff9fe3", "#ee89da", "#f078c7",
    "#ffa900", "#ff9000", "#ff6900",
    "#b0d3ee", "#7db1d9", "#5592c1",
    "#cee5c4", "#a6cd96", "#7bac66",
    "#ffe064", "#ffd839", "#ffcd00",
    "rgb(19,56,99,0.3)", "rgb(35,106,185,0.3)", "rgb(96,156,225,0.3)",
    "rgb(165,176,12,0.3)", "rgb(228,241,50,0.3)", "#F1F791",
    "#9D4502", "#FC7307", "#FEAB6D",
    "#29157E", "#4424D6", "#8C78E8",
    "#375F1B", "#66B032", "#9BD770",
    "#A70F01", "#FE2712", "#FE8176",
    "#B08A03", "#FCCB1A", "#FDE281"
  ];
  public lineChartLegend = true;
  public lineChartType = "line";

  @ViewChild(BaseChartDirective, { static: true }) chart: BaseChartDirective;
  @ViewChild("csvReader", { static: true}) csvReader: any;

  constructor(public dialog: MatDialog, public storage: AngularFireStorage) {
  }
  public records: any[] = [];

  uploadListener($event: any): void {
    const files = $event.srcElement.files;
    if (this.isValidCSVFile(files[0])) {
      const input = $event.target;
      const reader = new FileReader();
      reader.readAsText(input.files[0]);
      reader.onload = () => {
        const csvData = reader.result;
        const csvRecordsArray = ( csvData as string).split(/\r\n|\n/);
        this.records = this.getDataRecordsArrayFromCSVFile(csvRecordsArray);
      };
      reader.onerror = function() {
        console.log("error is occured while reading file!");
      };
      this.fileReset();
    } else {
      alert("Please import valid .csv file.");
      this.fileReset();
    }
  }

  getDataRecordsArrayFromCSVFile(csvRecordsArray: any) {
    const csvArr = [];
    const speed = [];
    const x = [];
    const y = [];
    const dates = [];

    for (let i = 1; i < csvRecordsArray.length-1; i++) {
      const currentRecord = ( csvRecordsArray[i] as string).split(",");
      speed.push(Number(currentRecord[3]));
      x.push(Number(currentRecord[4]) * 10);
      y.push(Number(currentRecord[5]) * 10);
      dates.push(currentRecord[6]);
    }

    this.lineChartData.push({data: speed, label: "Speed", backgroundColor: this.lineChartColors[ 0 + this.count * 3], borderColor: this.lineChartColors[ 0 + this.count * 3]});
    this.lineChartData.push({data: x, label: "X", backgroundColor: this.lineChartColors[ 1 + this.count * 3], borderColor: this.lineChartColors[ 1 + this.count * 3]});
    this.lineChartData.push({data: y, label: "Y", backgroundColor: this.lineChartColors[ 2 + this.count * 3], borderColor: this.lineChartColors[ 2 + this.count * 3]});
    console.log(this.count);
    this.count++;

    if (this.labels.length === 0) {
      for (let i = 1; i < dates.length; i++) {
        this.labels.push(dates[i]);
      }
    }

    this.maxspeed = Math.max(...speed);
    this.maxx = Math.max(...x);
    this.maxy = Math.max(...y);
    return csvArr;
  }

  isValidCSVFile(file: any) {
    return true;
  }

  getHeaderArray(csvRecordsArr: any) {
    const headers = ( csvRecordsArr[0] as string).split(",");
    const headerArray = [];
    for (let j = 0; j < headers.length; j++) {
      headerArray.push(headers[j]);
    }
    return headerArray;
  }

  ngOnInit(): void {
  }

  fileReset() {
    this.csvReader.nativeElement.value = "";
    this.records = [];
  }
}
