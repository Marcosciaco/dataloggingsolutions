export const PATH_HOME                      = "app";
export const PATH_LOGIN                     = "login";
export const PATH_FORMULAR                  = "form";
export const PATH_DATA                      = "data";
export const PATH_HELP                      = "help";
export const PATH_REGISTER                  = "register";
export const PATH_USER                      = "user";
export const PATH_MAP                       = "map";
export const PATH_PRIVACY                   = "privacy";
export const PATH_RECOVERY                  = "recovery";



