import { Component, OnInit } from "@angular/core";
import { FileService, Dat } from "../services/file.service";
import { AuthService } from "../services/auth.service";

@Component({
  selector: "app-files",
  templateUrl: "./files.component.html",
  styleUrls: ["./files.component.scss"]
})
export class FilesComponent implements OnInit {

  files: Dat[];

  constructor( public fileService: FileService, public auth: AuthService) { }

  ngOnInit() {
    this.auth.user$.subscribe(user => {
      this.fileService.getFiles().subscribe(file => {
        this.files = file.filter(file => file.user === user.uid);
      });
    });
  }
}
