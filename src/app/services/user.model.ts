export interface User {
    uid: string;
    email: string;
    photoURL?: string;
    displayName?: string;
    username: string;
    telefone: number;
    address: string;
    firstName: string;
    surName: string;
}
