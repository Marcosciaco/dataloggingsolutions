import { Injectable } from "@angular/core";
import { AngularFirestore, AngularFirestoreCollection } from "@angular/fire/firestore";
import { Observable } from "rxjs";
import { AuthService } from "./auth.service";

@Injectable({
  providedIn: "root"
})
export class FileService {

  filesCollection: AngularFirestoreCollection<Dat>;
  files: Observable<Dat[]>;

  constructor( public afs: AngularFirestore, public auth: AuthService) {
    this.files = this.afs.collection("files").valueChanges();
  }

  getFiles() {
    return this.files;
  }

  filterFiles(): Array<Dat> {
    let ret: Dat[];
    this.auth.user$.subscribe(user => {
      this.files.subscribe(file => {
        ret = file.filter(file => user.uid === file.user);
      });
    });

    return ret;
  }
}

export interface Dat {
  downloadURL?: string;
  path?: string;
  user?: string;
}
