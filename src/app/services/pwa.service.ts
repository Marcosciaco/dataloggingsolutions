import { Injectable } from '@angular/core';
import { UpdateAvailableEvent, SwUpdate } from '@angular/service-worker';
import { ReplaySubject, Observable } from 'rxjs';
import { EventManager } from '@angular/platform-browser';

@Injectable({
  providedIn: 'root'
})
export class PwaService {

  private _update = new ReplaySubject<UpdateAvailableEvent>(1);
  private _defferedPrompt = new ReplaySubject<any>(1);

  constructor(
    swUpdate: SwUpdate,
    eventManager: EventManager
  ) {
    eventManager.addGlobalEventListener("window", "beforeinstallprompt", e => {
      e.preventDefault();
      e.userChoice.then(() => {
        this._defferedPrompt.unsubscribe();
        this._defferedPrompt = new ReplaySubject(1);
      });
      this._defferedPrompt.next(e);
    });
    swUpdate.available.subscribe(e => this._update.next(e));
  }

  public get update(): Observable<UpdateAvailableEvent> {
    return this._update;
  }

  public get defferedPrompt(): Observable<any> {
    return this._defferedPrompt;
  }
}
