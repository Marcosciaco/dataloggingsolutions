import { Injectable } from "@angular/core";
import { Router } from "@angular/router";

import { auth } from "firebase/app";
import { AngularFireAuth } from "@angular/fire/auth";
import { AngularFirestore, AngularFirestoreDocument } from "@angular/fire/firestore";

import { Observable, of, from } from "rxjs";
import { switchMap } from "rxjs/operators";
import { User } from "./user.model";
import * as firebase from "firebase";
import { MatSnackBar } from "@angular/material/snack-bar";

@Injectable({
  providedIn: "root"
})
export class AuthService {

  user$: Observable<User>;

  constructor(
    private afAuth: AngularFireAuth,
    private afs: AngularFirestore,
    private router: Router,
    private snackBar: MatSnackBar
  ) {
    this.user$ = this.afAuth.authState.pipe(
      switchMap(user => {
        if (user) {
          return this.afs.doc<User>(`users/${user.uid}`).valueChanges();
        } else {
          return of(null);
        }
      })
    );
   }

  AuthLogin(provider) {
    return this.afAuth.auth.signInWithPopup(provider)
    .then((result) => {
        console.log("You have been successfully logged in!");
    }).catch((error) => {
        console.log(error);
    });
  }

  async fbLogin() {
    const provider = new auth.FacebookAuthProvider();
    const credential = await this.afAuth.auth.signInWithPopup(provider);
    this.updateUserData(credential.user);
    return this.router.navigate(["/app"]);
  }

  async googleSignin() {
     const provider = new auth.GoogleAuthProvider();
     const credential = await this.afAuth.auth.signInWithPopup(provider);
     //console.log(credential.additionalUserInfo);
     //console.log(credential.user);
     return this.router.navigate(["/app"]);
  }

   async signOut() {
     await this.afAuth.auth.signOut();
     return this.router.navigate(["/login"]);
   }

   // Sign up with email/password
  async SignUp(email, password, username, tel, address, name, surname) {
    return this.afAuth.auth.createUserWithEmailAndPassword(email, password)
      .then((result) => {
        this.snackBar.open("Registered", "OK", {
          duration: 2000,
        });
        this.updateUserData(result.user, username, tel, address, name, surname);
        console.log(result.user);
        return this.router.navigate(["/login"]);
      }).catch((error) => {
        window.alert(error.message);
      });
  }

  // Sign in with email/password
  public SignIn(email, password) {
    return this.afAuth.auth.signInWithEmailAndPassword(email, password)
      .then((result) => {
         this.router.navigate(["/app"]);
      }).catch((error) => {
        window.alert(error.message);
      });
  }


   public updateUserData(user, usern?, tel?, addr?, name?, surname?) {
    const userRef: AngularFirestoreDocument<User> = this.afs.doc(`users/${user.uid}`);

    const data = {
      uid: user.uid,
      email: user.email,
      displayname: user.displayName,
      photoURL: user.photoURL,
      username: usern,
      telefone: tel,
      address: addr,
      firstName: name,
      surName: surname
    };

    return userRef.set(data, {merge: true});
   }

   public resetPassword(email: string) {
     const auth = firebase.auth();
     return auth.sendPasswordResetEmail(email).then(() => console.log("email sent")).catch((error) => console.log(error));
   }
}
