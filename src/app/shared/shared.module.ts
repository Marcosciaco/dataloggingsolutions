import { NgModule } from "@angular/core";
import { MatButtonModule } from "@angular/material/button";
import { MatCardModule } from "@angular/material/card";
import { MatGridListModule } from "@angular/material/grid-list";
import { MatCheckboxModule } from "@angular/material/checkbox";
import { MatToolbarModule } from "@angular/material/toolbar";
import { MatIconModule } from "@angular/material/icon";
import { MatExpansionModule } from "@angular/material/expansion";
import { MatInputModule } from "@angular/material/input";
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatSelectModule } from "@angular/material/select";
import { NgxChartsModule } from "@swimlane/ngx-charts";
import { MatChipsModule } from "@angular/material/chips";
import { MatProgressBarModule } from "@angular/material/progress-bar";
import { MatStepperModule } from "@angular/material/stepper";
import { jqxChartModule } from "jqwidgets-ng/jqxchart";
import { MatTabsModule } from "@angular/material/tabs";
import { HttpClientModule } from "@angular/common/http";
import { MatDialogModule } from "@angular/material/dialog";
import { MatMenuModule } from "@angular/material/menu";
import { FormsModule } from "@angular/forms";
import { MatSidenavModule } from "@angular/material/sidenav";
import { AvatarModule } from "ngx-avatar";
import { MatBottomSheetModule } from "@angular/material/bottom-sheet";
import { MatListModule } from "@angular/material/list";
import { MatSnackBarModule } from "@angular/material/snack-bar";

@NgModule({
    exports: [
        MatMenuModule,
        MatSnackBarModule,
        MatButtonModule,
        MatSidenavModule,
        MatBottomSheetModule,
        jqxChartModule,
        MatTabsModule,
        MatCardModule,
        MatCheckboxModule,
        MatListModule,
        MatExpansionModule,
        MatFormFieldModule,
        MatChipsModule,
        MatGridListModule,
        MatIconModule,
        MatInputModule,
        MatSelectModule,
        MatToolbarModule,
        NgxChartsModule,
        MatProgressBarModule,
        MatStepperModule,
        HttpClientModule,
        MatDialogModule,
        FormsModule,
        AvatarModule,
    ],
    declarations: []
})
export class SharedModule {
}
