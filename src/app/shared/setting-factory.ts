import { Setting } from "./setting";

export class SettingFactory {
    constructor() {}
    static empty(): Setting {
        return new Setting(new Date(), "", "", "", 0, "", 0, "",0, 0, 0, "", "", "", [{name: "", value: ""}]);
    }
}
