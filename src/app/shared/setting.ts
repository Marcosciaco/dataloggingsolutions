import { Custom } from "./custom";

export class Setting {

    constructor(
        public datum: Date,
        public ort: string,
        public beschreibung: string,
        public wetter: string,
        public temp: number,
        public laufzeit: string,
        public platz: number,
        public reifen: string,
        public gewicht: number,
        public luftdruckhinten: number,
        public luftdruckvorne: number,
        public daempferVorn: string,
        public daempferHinten: string,
        public notizen: string,
        public custom: Custom[]
    ) {}
}
