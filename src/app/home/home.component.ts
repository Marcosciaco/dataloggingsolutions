import { Component, OnInit, ViewChild, ChangeDetectorRef } from "@angular/core";
import { AuthService } from "src/app/services/auth.service";
import { Chart, ChartDataSets, ChartOptions } from "chart.js";
import { Label, Color, BaseChartDirective } from "ng2-charts";
import { PwaService } from '../services/pwa.service';


@Component({
  selector: "app-home",
  templateUrl: "./home.component.html",
  styleUrls: ["./home.component.scss"]
})
export class HomeComponent implements OnInit {
  
  public updateAvailable: boolean;
  public defferedPrompt: any;

  constructor(
    private cdRef: ChangeDetectorRef,
    private pwaService: PwaService,  
    public auth: AuthService
  ) {
  }

  ngOnInit() {
    this.pwaService.update.subscribe(e => {
      this.updateAvailable = true;
      this.cdRef.markForCheck();
    });

    this.pwaService.defferedPrompt.subscribe(e => {
      this.defferedPrompt = e;
      this.cdRef.markForCheck();
    });
  }

  public refresh() {
    location.reload();
  }
}
